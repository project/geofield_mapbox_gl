<?php

namespace Drupal\Tests\geofield_mapbox_gl\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group geofield_mapbox_gl
 */
class LoadTest extends BrowserTestBase
{

    /**
     * Modules to enable.
     *
     * @var array
     */
    public static $modules = ['geofield_mapbox_gl'];

    /**
     * A user with permission to administer site configuration.
     *
     * @var \Drupal\user\UserInterface
     */
    protected $user;

    /**
     * {@inheritdoc}
     */
    protected function setUp() 
    {
        parent::setUp();
        $this->user = $this->drupalCreateUser(['administer site configuration']);
        $this->drupalLogin($this->user);
    }

    /**
     * Tests that the home page loads with a 200 response.
     */
    public function testLoad() 
    {
        $this->drupalGet(Url::fromRoute('<front>'));
        $this->assertSession()->statusCodeEquals(200);
    }

}

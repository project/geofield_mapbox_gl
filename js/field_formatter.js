(function ($) {
    Drupal.behaviors.geofield_mapbox_gl_field_formatter = {
        attach: function (context, settings) {
            if (!(context === window.document)) {
                return;
            }
            $(document).on(
                'new-layer', function (event, map, mapData, mapId, index, layerData) {
                    map.loadImage(
                        settings.geofield_mapbox_gl.marker_image, function (error, image) {
                            if (error) { throw error;
                            }
                            map.addImage('marker_image', image);
                        }
                    );
                    layerData.source.data.features.forEach(
                        function (point) {
                            if (settings.geofield_mapbox_gl.marker_style == "marker") {
                                var marker = new mapboxgl.Marker({})
                                .setLngLat(point['geometry'].coordinates)
                                .setPopup(
                                    new mapboxgl.Popup({ offset: 25 })
                                    .setHTML(
                                        '<h2><a href="' +point.properties.geourl+ '" target="_blank">Go there</a></h2>' +
                                        point.properties.description
                                    )
                                )
                                .addTo(map)
                            }
                        }
                    )
                    map.on(
                        'click', 'points', function (e) {
                            var coordinates = e.features[0].geometry.coordinates.slice();
                            var description = e.features[0].properties.description + 
                            '<h2><a href="' +e.features[0].properties.geourl+ '" target="_blank">Open in app</a></h2>'
                            // Ensure that if the map is zoomed out such that multiple
                            // copies of the feature are visible, the popup appears
                            // over the copy being pointed to.
                            while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                                coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
                            }
                            new mapboxgl.Popup()
                            .setLngLat(coordinates)
                            .setHTML(description)
                            .addTo(map);
                        }
                    )
                }
            )
            $(document).on(
                'new-source', function (event, map,mapData, mapId, sourceId, sourceData) {
                    console.log(sourceData)
                }
            )
            $(document).on(
                "mapbox-gl.map", function ( event, map, mapData, mapId ) {
     
                    map.on(
                        'data', function (data) {
                            console.log(data.dataType)
                            if (data.dataType === 'source' && data.isSourceLoaded) {
                                console.log('data loaded', data)
                                   return;
                                // create a HTML element for each feature
                                var el = document.createElement('div');
                                el.className = 'marker';
                                // make a marker for each feature and add to the map
                                new mapboxgl.Marker(el)
                                .setLngLat(point['geometry'].coordinates)
                                .setPopup(
                                    new mapboxgl.Popup({ offset: 25 }) // add popups
                                    .setHTML('<a href="' +point.properties.url+ '"><img class="fitxa-entitat logo-up" src="' + point.properties.image + '" height="48px" width="48px"/><h3>' + point.properties.title + '</h3><p></p></a><p></p>')
                                )
                                .addTo(map);
                            }
                        }
                    )
                }
            );
        }
    };
})(jQuery);

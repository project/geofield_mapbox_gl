CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This will add a new field formatter as map in a geofield field, using
the mapbox services.
The module https://www.drupal.org/project/mapbox_gl is mandatory.
After enabled you can choose the formatter of a geofield as a "mapbox gl",
in manage display of any entity that has a geofield field.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/geofield_mapbox_gl

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/geofield_mapbox_gl


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Geofield mapbox gl module as you would normally install
   a contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Structure > Content type > Manage fields >
       Add a new field "Geofield".
    3. Navigate to Administration > Structure > Content type > Manage display >
       and choose "Mapbox GL" Format.
    4. To make it work you have to obtain an API key from mapbox. And then choose
       the representation of the point:
        * Marker: https://docs.mapbox.com/mapbox-gl-js/example/marker-from-geocode
        * Circle: https://docs.mapbox.com/mapbox-gl-js/example/multiple-geometries
        * Icon: https://docs.mapbox.com/mapbox-gl-js/example/geojson-markers
        * Custom image: https://docs.mapbox.com/mapbox-gl-js/example/add-image
        * Custom Image from other field in the same entity, with the choosen image
          view mode.
        * "recursive str_replace": If any error of recursive str_replace happens,
          make it sure that an appropiate view mode is used,with special focus
          that the view mode is not using the geofield_mapbox_gl formatter
          again and again.
        * tooltips: Every representated point comes with a tooltip(as
          as https://docs.mapbox.com/mapbox-gl-js/example/popup-on-click/)
          with the choosen view mode of the same entity.


MAINTAINERS
-----------

 * Aleix Quintana (aleix) - https://www.drupal.org/u/aleix

Supporting organizations:
 * Communia - https://www.drupal.org/communia

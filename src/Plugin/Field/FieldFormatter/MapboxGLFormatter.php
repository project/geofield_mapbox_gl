<?php

namespace Drupal\geofield_mapbox_gl\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\geofield\DmsConverter;
use Drupal\geofield\Plugin\Field\FieldFormatter\GeofieldDefaultFormatter;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\FormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\geofield\GeoPHP\GeoPHPInterface;
use Drupal\image\Entity\ImageStyle;


/**
 * Plugin implementation of the 'geofield_latlon' formatter.
 *
 * @FieldFormatter(
 *   id = "geofield_mapbox_gl",
 *   label = @Translation("Mapbox GL"),
 *   field_types = {
 *     "geofield"
 *   }
 * )
 */
class  MapboxGLFormatter extends FormatterBase implements ContainerFactoryPluginInterface
{

    /**
     * The array that will carry all the properties to load a map.
     *
     * @var array
     */
    protected $map;

    /**
     * The geoPhpWrapper service.
     *
     * @var \Drupal\geofield\GeoPHP\GeoPHPInterface
     */
    protected $geoPhpWrapper;

    /**
     * The Adapter Map Options.
     *
     * @var array
     */
    protected $options;

    /**
     * The entity type manager.
     *
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    protected $entityTypeManager;

    /**
     * The entity display repository.
     *
     * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
     */
    protected $entityDisplayRepository;

    /**
     * The entity field manager.
     *
     * @var \Drupal\Core\Entity\EntityFieldManagerInterface
     */
    protected $entityFieldManager;

    /**
     * The current view mode.
     *
     * @var string
     */
    protected $viewMode;

    /**
     * GeofieldDefaultFormatter constructor.
     *
     * @param string                                               $plugin_id
     *   The plugin_id for the formatter.
     * @param mixed                                                $plugin_definition
     *   The plugin implementation definition.
     * @param \Drupal\Core\Field\FieldDefinitionInterface          $field_definition
     *   The definition of the field to which the formatter is associated.
     * @param array                                                $settings
     *   The formatter settings.
     * @param string                                               $label
     *   The formatter label display setting.
     * @param string                                               $view_mode
     *   The view mode.
     * @param array                                                $third_party_settings
     *   Any third party settings settings.
     * @param \Drupal\geofield\GeoPHP\GeoPHPInterface              $geophp_wrapper
     *   The geoPhpWrapper.
     * @param \Drupal\Core\Entity\EntityTypeManager                $entity_type_manager
     *   The entity type manager.
     * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface
     *   The entity display repository.
     * @param \Drupal\Core\Entity\EntityFieldManagerInterface
     *   The  entity field manager.
     */
    public function __construct(
        $plugin_id,
        $plugin_definition,
        FieldDefinitionInterface $field_definition,
        array $settings,
        $label,
        $view_mode,
        array $third_party_settings,
        GeoPHPInterface $geophp_wrapper,
        EntityTypeManager $entity_type_manager,
        EntityDisplayRepositoryInterface $entity_display_repository,
        EntityFieldManagerInterface $entity_field_manager
    ) {
        parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $entity_type_manager, $entity_display_repository,  $entity_field_manager);
        $this->geoPhpWrapper = $geophp_wrapper;
        $this->options = $this->geoPhpWrapper->getAdapterMap();
        $this->entityTypeManager = $entity_type_manager;
        $this->entityDisplayRepository = $entity_display_repository;
        $this->entityFieldManager = $entity_field_manager;
        $this->viewMode = $view_mode;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) 
    {
        return new static(
            $plugin_id,
            $plugin_definition,
            $configuration['field_definition'],
            $configuration['settings'],
            $configuration['label'],
            $configuration['view_mode'],
            $configuration['third_party_settings'],
            $container->get('geofield.geophp'),
            $container->get('entity_type.manager'),
            $container->get('entity_display.repository'),
            $container->get('entity_field.manager')
        );
    }

    /**
     * Gets the current view mode.
     *
     * @return string
     *   the current view mode
     */
    protected function getViewMode()
    {
        return $this->viewMode;
    }

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings() 
    {
        return [
        'mapbox_api_key' => '',
        'style' => 'mapbox://styles/mapbox/light-v9',
        'zoom' => 10,
        'marker_style' => 'symbol',
        'circle_radius' => 10,
        'circle_color' => '#ff0000',
        'icon' => 'marker',
        'tooltip_view_mode' => 'default',
        'custom_icon_path' => '',
        'icon_field' => '',
        'icon_field_image_style' => '',
        ] + parent::defaultSettings();
    }

    /**
     * Helper function to get the formatter settings options.
     *
     * @return array
     *   The formatter settings options.
  protected function formatOptions() {
    return [
      'decimal' => $this->t("Decimal Format (17.76972)"),
      'dms' => $this->t("DMS Format (17° 46' 11'' N)"),
      'dm' => $this->t("DM Format (17° 46.19214' N)"),
    ];
  }
     */

    /**
     * Returns the output format, set or default one.
     *
     * @return string
     *   The output format string.
  protected function getOutputFormat() {
    return in_array($this->getSetting('output_format'), array_keys($this->formatOptions())) ? $this->getSetting('output_format') : self::defaultSettings()['output_format'];
  }
     */

    /**
     * Gets all image fields of the current bundle.
     *
     * @param array  $entity_type_id
     *   The entity type id.
     *
     * @param string $bundle_type_id
     *   The bundle type id.
     *
     * @return array
     *   The fields' machinename that are of type image in that bundle
     */
    protected function getImageFields($entity_type_id, $bundle_type_id)
    {
        $image_fields = $this->entityFieldManager->getFieldMapByFieldType('image');
        $fields_names = [];
        if (isset($image_fields[$entity_type_id])) {
            foreach ($image_fields_in_entity as $field_name => $type_and_bundles){
                if (in_array($bundle_type_id, $type_and_bundles['bundles'])) {
                    $fields_names[] = [ $field_name => $field_name ];
                }
            }
        }
        return $fields_names;
    }
    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state) 
    {
        $elements = parent::settingsForm($form, $form_state);
        $entity_display_repository = \Drupal::service('entity_display.repository'); //TODO DI
    
        $bundle_type_id = $this->fieldDefinition->getTargetBundle();
        $entity_type_id = $this->fieldDefinition->getTargetEntityTypeId();
        $image_fields_in_bundle = $this->getImageFields($entity_type_id, $bundle_type_id);
        $field_name = $this->fieldDefinition->getName();

        //$form_state;
        //[settings][marker_style]

        $elements['mapbox_api_key'] = [
        '#title' => $this->t('Mapbox api key'),
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('mapbox_api_key')
        ];
        $elements['style'] = [
        '#title' => $this->t('Style'),
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('style'),
        '#required' => true,
        ];
        $elements['zoom'] = [
        '#type' => 'number',
        '#min' => 0,
        '#max' => 20,
        '#title' => $this->t('Map zoom'),
        '#default_value' => $this->getSetting('zoom'),
        ];
        $elements['marker_style'] = [
        '#type' => 'select',
        '#title' => $this->t('Marker style'),
        '#default_value' => $this->getSetting('marker_style'),
        '#options' => [
        'marker' => 'Marker',
        'circle' => 'Circle',
        'symbol' => 'Symbol',
        'image' => 'Custom image',
        'image_field' => 'Image from field',
        /*
        TODO
        'html' => 'Html',
        'd3' => 'D3'*/
        ]
        ];
        $elements['circle_radius'] = [
        '#type' => 'number',
        '#min' => 0,
        '#title' => $this->t('Circle radius'),
        '#default_value' => $this->getSetting('circle_radius'),
        '#states' => [
        'visible' => [
          "select[name=\"fields[$field_name][settings_edit_form][settings][marker_style]\"]" => ['value' => 'circle'],
        ]
        ]
        ];
        $elements['circle_color'] = [
        '#type' => 'color',
        '#default_value' => $this->getSetting('circle_color'),
        '#title' => $this->t('Circle color'),
        '#states' => [
        'visible' => [
          "select[name=\"fields[$field_name][settings_edit_form][settings][marker_style]\"]" => ['value' => 'circle'],
        ]
        ]
        ];
        $elements['icon'] = [
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('icon'),
        '#description' => $this->t('Take icon names from maki icons from mapbox'),
        '#states' => [
        'visible' => [
          "select[name=\"fields[$field_name][settings_edit_form][settings][marker_style]\"]" => ['value' => 'symbol'],
        ]
        ]
        ];
        $elements['custom_icon_path'] = [
        '#type' => 'url',
        '#title' => $this->t('Custom icon path'),
        '#default_value' => $this->getSetting('custom_icon_path'),
        '#description' => $this->t('The path to set the marker icon from image (take care with dimensions).'),
        '#states' => [
        'visible' => [
          "select[name=\"fields[$field_name][settings_edit_form][settings][marker_style]\"]" => ['value' => 'image'],
        ]
        ]
        ];
        $elements['custom_icon_field'] = [
        '#type' => 'select',
        '#title' => $this->t('Custom icon field'),
        '#default_value' => $this->getSetting('icon_field'),
        '#description' => $this->t('The field that holds the marker icon.'),
        '#options' => $image_fields_in_bundle,
        '#states' => [
        'visible' => [
          "select[name=\"fields[$field_name][settings_edit_form][settings][marker_style]\"]" => ['value' => 'image_field'],
        ]
        ]
        ];
        $elements['custom_icon_field_image_style'] = [
        '#type' => 'select',
        '#title' => $this->t('Image style'),
        '#default_value' => $this->getSetting('icon_field_image_style'),
        '#description' => $this->t('The image style that will be used for the image in the specified field.'),
        '#options' => array_keys($this->entityTypeManager->getStorage('image_style')->loadMultiple()),
        '#states' => [
        'visible' => [
          "select[name=\"fields[$field_name][settings_edit_form][settings][marker_style]\"]" => ['value' => 'image_field'],
        ]
        ]
        ];
        $view_modes = $this->entityDisplayRepository->getViewModeOptionsByBundle($entity_type_id, $bundle_type_id);
        unset($view_modes[$this->getViewMode()]);

        $elements['tooltip_view_mode'] = [
        '#type' => 'select',
        '#title' => $this->t('Tooltip view mode'),
        '#default_value' => $this->getSetting('tooltip_view_mode'),
        '#options' => [ "none" => "none" ] + $view_modes,
        ];
        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary() 
    {
        $summary[] = $this->t('Output format: @style at zoom @zoom', ['@style' => $this->getSetting('style'), '@zoom' => $this->getSetting('zoom')]);
        return $summary;
    }

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode) 
    {
        $content = [];
        $layer_type = 'circle';
        switch ($this->getSetting('marker_style')){
        case 'marker':
            $layer_type = 'circle';
            break;
        case 'image':
        case 'symbol':
        case 'image_field':
            $layer_type = 'symbol';
            break;
        default:
            $layer_type = 'circle';
        }
        if (count($items) > 0 ) {
            $this->map =  [
            'Streets' =>
            [
            'access_token' => $this->getSetting('mapbox_api_key'),
            'options' => [
            'container' => 'mapbox-streets',
            //'style' => 'https://openmaptiles.github.io/toner-gl-style/style-cdn.json',
            'style' => $this->getSetting('style'),
            'zoom' => $this->getSetting('zoom'),
            //'center' => [2.15, 41.555],
            ],
            'config' =>
            [
            //'height' => '400px',
            //'popup' => 'popup',
            'controls' =>
            [
              'AttributionControl' => [
                'compact' => true
              ],
            ]
            ],
            'layers' =>
            [
            [
              'id' => 'points',
              'type' => $layer_type,
              'source' => [
                'type' => 'geojson',
                'data' => [
                  'type' => 'FeatureCollection',
                  // to be filled by items values
                  'features' => [],
                ]
              ],
              //'cluster' => True,
              'filter' => ['any', [ '==', '$type', 'Point']],
            ],
            [
              'id' => 'polygons',
              'type' => 'fill',
              'paint' => [
                'fill-color' => 'rgba(200, 0, 0, 0.4)',
                'fill-outline-color' => 'rgba(200, 0, 0, 1)',
              ],
              'source' => [
                'type' => 'geojson',
                'data' => [
                  'type' => 'FeatureCollection',
                  // to be filled by items values
                  'features' => [],
                ]
              ],
              //'cluster' => True,
              'filter' => ['any', [ '!=', '$type', 'Point']],
            ],



                     /*[
                       'id' => 'Entitats',
                       'type' => 'circle',
                       'source' => [
                         'type' => 'geojson',
                         'data' => $this->entitiesUrl
                       ],
                       'paint' => [
                         'circle-color' => '#f47c1a',
                         //'circle-blur' => 0.85,
                         'circle-radius' => 7,
                       ],

                      */
            ]
            ],
            // For options, see: https://www.mapbox.com/mapbox-gl-js/api/
            ];
            if ($layer_type === "circle") {
                $this->map['Streets']['layers'][0]['paint'] = [
                'circle-radius' => (int)$this->getSetting('circle_radius'),
                'circle-color' => $this->getSetting('circle_color'),
                ];
            }
            // if marker_style == symbol or image
            $content['#attached']['drupalSettings']['geofield_mapbox_gl']['marker_image'] = "";
            if ($layer_type === "symbol") {
                switch($this->getSetting("marker_style")){
                case "image_field":
                    $styled_image_url = ImageStyle::load($this->getSetting('custom_icon_field_image_style'))->buildUrl($node->{$this->getSetting('custom_icon_field')}->entity->getFileUri());
                    $content['#attached']['drupalSettings']['geofield_mapbox_gl']['marker_image'] = $styled_image_url;
                    break;
                case "image":
                    $content['#attached']['drupalSettings']['geofield_mapbox_gl']['marker_image'] = $this->getSetting('custom_icon_path');
                    break;
                }

                $this->map['Streets']['layers'][0]['layout'] = [
                "icon-image" => ($this->getSetting('marker_style') === "symbol") ? "{icon}-15" : "marker_image",
                "icon-size" => 1,
                "text-field" => "{title}",
                "text-font" => ["Open Sans Semibold", "Arial Unicode MS Bold"],
                "text-offset" => [0, 0.6],
                "text-anchor" => "top"

                ];
            }
            // if marker_style == marker ()
            if ($this->getSetting('marker_style') === "marker") {
                $this->map['Streets']['layers'][0]['paint'] = [
                'circle-radius' => 0,
                ];
            }
            $content['#attached']['drupalSettings']['geofield_mapbox_gl']['marker_style'] = $this->getSetting('marker_style');
      
            foreach ($items as $delta => $item) {
                $this->addFeature($item, $delta);
            }

            // Add features as a source and after add layer with that source
            /*
            $points_source = [
                //'id' => 'points_source',
                'type' => 'geojson',
                'data' => [
                  'type' => 'FeatureCollection',
                  // to be filled by items values
                  'features' => [

                  ],
                ]
              ];
            $sourced_layer = [
              'id' => 'points_sourced',
              'type' => 'symbol',
              'source' => 'points_source',
              //'cluster' => True,
              'layout' => [
                "icon-image" => "{icon}-15",
                "icon-size" => 3,
                "text-field" => "{title}",
                "text-font" => ["Open Sans Semibold", "Arial Unicode MS Bold"],
                "text-offset" => [0, 0.6],
                "text-anchor" => "top"
              ]
            ];
            foreach ($items as $delta => $item) {
            //add as source
            $this->addFeature($item, $delta, $points_source);
            }

            $this->map['Streets']['layers'][] = $sourced_layer;
            $this->map['Streets']['sources']['points_source'] = $points_source;
            */

            $map_info = $this->map['Streets'];
            $content['mapbox-container'] = [
            '#prefix' => '<div class="mapbox-gl-wrapper">',
            '#markup' => '<div id="'  .  $map_info['options']['container'] . '-menu" class="mapbox-gl-layer-menu"></div>'
            . '<div id="' .  $map_info['options']['container'] . '" class="mapbox-gl-container"></div>',
            '#suffix' => '</div">'
            ];

            $sources = key_exists('sources', $map_info) ? $map_info['sources']: [];
            $layers =  key_exists('layers', $map_info) ? $map_info['layers']: [];

            $settings[$map_info['options']['container']] = [
            'accessToken' => $map_info['access_token'],
            'options' => $map_info['options'],
            'sources' => $sources,
            'layers' => $layers,
            'config' => $map_info['config']
            ];

            global $base_url;
            $content['#attached']['drupalSettings']['baseUrl'] = $base_url;
            $content['#attached']['drupalSettings']['mapboxGl'] = $settings;
            //$content['#attached']['library'][] = 'mapbox_show/mapbox_show-form';
            //$content['#attached']['library'][] = 'mapbox_show/d3.d3js';
            $content['#attached']['library'][] = 'geofield_mapbox_gl/field_formatter.mapbox_gl';
            $content['#attached']['library'][] = 'mapbox_gl/libraries.mapbox-gl-js';
            $content['#attached']['library'][] = 'mapbox_gl/mapbox_gl';
        }
        return [$content];
    }

    /**
     * Appends a feature point from geofield field item to the map
     *
     * @param \Drupal\Core\Field\FieldItemInterface $field_item
     *   The field item.
     * @param int                                   $delta
     *   The item delta in the fielditemlist
     * @param array                                 $source
     *   Optionally add to source and append this source to the layer
     */
    protected function addFeature(FieldItemInterface $item, int $delta, array $source = null)
    {
        // dirty way to prevent recursion...
        if(memory_get_peak_usage() > 268435400) {
            \Drupal::logger('geofield_mapbox_gl')->error("recursion of rendering entity that renders the same entity view mode"); // TODO DI
            return;
        }
        $geom = $this->geoPhpWrapper->load($item->value);
        if ($geom && $geom->getGeomType() == 'Point') {
            /* @var \Point $geom */
            $entity_type = $this->fieldDefinition->getTargetEntityTypeId();
            $entity = $item->getEntity();
            $view_entity = $this->entityTypeManager->getViewBuilder($entity_type)->view($entity, $this->getSetting('tooltip_view_mode'));
            $rendered_entity = \Drupal::service('renderer')->render($view_entity); // TODO DI
            $this->map['Streets']['options']['center'] = [$geom->x(), $geom->y()];
            $feature = [
            'type' => 'Feature',
            'geometry' => [
            'type' => 'Point',
            'coordinates' =>  [ $geom->x(), $geom->y()]
            ],
            'properties' => [
            'title' => $entity->label() . " #" .$delta,
            'description' => ($this->getSetting('tooltip_view_mode') == 'none')? "" : $rendered_entity,// Url::fromUri('geo:@lat,@lon', ['@lat' => $geom->y(), '@lon' => $geom->x()])),
            'geourl' => "geo:" . $geom->x() . "," . $geom->y(),
            'icon' => $this->getSetting('icon'),
            ]
            ];

            if (!$source) {
                $this->map['Streets']['layers'][0]['source']['data']['features'][] = $feature;
            }
            else {
                $source ['data']['features'][] = $feature;
            }
        }
       if ($geom && $geom->getGeomType() == 'Polygon') {
            $this->map['Streets']['options']['center'] = [$geom->getCentroid()->getX(), $geom->getCentroid()->getY()];
            $feature = [
            'type' => 'Feature',
            'geometry' => [
            'type' => 'Polygon',
            'coordinates' =>  $geom->asArray()
            ],
            /*'properties' => [
              'title' => $entity->label() . " #" .$delta,
              'description' => ($this->getSetting('tooltip_view_mode') == 'none')? "" : $rendered_entity,// Url::fromUri('geo:@lat,@lon', ['@lat' => $geom->y(), '@lon' => $geom->x()])),
              'geourl' => "geo:" . $geom->x() . "," . $geom->y(),
              'icon' => $this->getSetting('icon'),
            ]*/
            ];

            if (!$source) {
                //unset($this->map['Streets']['layers'][0]['layout']);
                $this->map['Streets']['layers'][1]['source']['data']['features'][] = $feature;
            }
            else {
                $source ['data']['features'][] = $feature;
            }
        }

    }
}
